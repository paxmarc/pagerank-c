#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <stdint.h>

#include "pagerank.h"

void pagerank(list* plist, int ncores, int npages, int nedges, double dampener) {

    int edgeIndex = 0;
    int endIndex[npages], startIndex[npages], outlinks[npages], edges[nedges];
    char** names = malloc(npages * sizeof(char*));

    double division[npages];

    int index;

    for (node* n = plist->head; n; n = n->next) {
        index = n->page->index;
        outlinks[index] = n->page->noutlinks;
        division[index] = 1.0 / outlinks[index];
        startIndex[index] = edgeIndex;
        names[index] = n->page->name;
        
        if(n->page->inlinks != NULL) {
            for (node* m = n->page->inlinks->head; m != NULL; m = m->next) {
                edges[edgeIndex++] = m->page->index;
            }
        }
        endIndex[index] = edgeIndex;
    }

    double *pagerank_t = malloc(sizeof(double) * npages);
    double *pagerank_t_next = malloc(sizeof(double) * npages);

    double dampen_av = (1.0 - dampener)/ npages;
    double original_value = 1.0 / npages;

    for (int i = 0; i < npages; i++) pagerank_t[i] = original_value;

    double sumOfSquares;
    do {

        sumOfSquares = 0;
        for (int i = 0; i < npages; i++) {
            double toAdd = 0;
            for(int j = startIndex[i]; j < endIndex[i]; j++) {
                toAdd += (pagerank_t[edges[j]] * division[edges[j]]);
            }
            pagerank_t_next[i] = dampen_av + (dampener * toAdd);

            sumOfSquares += ((dampen_av + (dampener * toAdd))  - pagerank_t[i]) * ((dampen_av + (dampener * toAdd)) - pagerank_t[i]);

        }

        double* temp = pagerank_t;
        pagerank_t = pagerank_t_next;
        pagerank_t_next = temp;

    } while(sumOfSquares >= EPSILON * EPSILON);

    //Print out the scores
    for (int i = 0; i < npages; i++) {
        printf("%s %.4lf\n", names[i], pagerank_t[i]);
    }

    free(pagerank_t);
    free(pagerank_t_next);
    free(names);
}


/*
######################################
### DO NOT MODIFY BELOW THIS POINT ###
######################################
*/

int main(void) {

    /*
######################################################
### DO NOT MODIFY THE MAIN FUNCTION OR HEADER FILE ###
######################################################
*/

    list* plist = NULL;

    double dampener;
    int ncores, npages, nedges;

    /* read the input then populate settings and the list of pages */
    read_input(&plist, &ncores, &npages, &nedges, &dampener);

    /* run pagerank and output the results */
    pagerank(plist, ncores, npages, nedges, dampener);

    /* clean up the memory used by the list of pages */
    page_list_destroy(plist);

    return 0;
}
