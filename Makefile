###############################
### DO NOT MODIFY THIS FILE ###
###############################

CC = clang
CFLAGS = -g -O0 -W -Wall -pedantic -std=gnu11 -march=native
LDFLAGS = -lm -lpthread

HASTE = /labcommon/comp2129/assignment4/bin/haste

.PHONY: all clean haste test rain update submission

all: pagerank
	@$(HASTE) --update

pagerank: pagerank.o

clean:
	-rm -f *.o
	-rm -f pagerank

haste:
	@$(HASTE) --all

test:
	@$(HASTE) --test

rain:
	@$(HASTE) --rain

update:
	@$(HASTE) --update

submission:
	@$(HASTE) --submit
